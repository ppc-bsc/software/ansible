# Public Ansible playbooks used at PPC
In this repository you can find the Ansible projects used in the group. You can install Ansible executing the following commands:
```
apt update
apt install ansible ansible-core
```

Links of interest:
- Introduction to Ansible: https://docs.ansible.com/ansible/latest/getting_started/introduction.html
- Learning Ansible basics: https://www.redhat.com/en/topics/automation/learning-ansible-tutorial

Ansible projects normally have the following structure:
- Inventories
- Playbooks
- Roles

### Inventories
An inventory is a configuration file that defines the hosts and groups of hosts on which Ansible operations should be performed. 

Official guide on how to build an inventory: https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html

### Playbooks
Playbooks are configuration files written in YAML format that define a set of tasks (or roles) and configurations to be executed on the remote hosts defined in inventories. 

### Tasks
Tasks in Ansible define specific actions or operations that are performed on a target group of hosts. They are the fundamental unit of work within a playbook or role and each is responsible for executing a single action, such as installing a package, copying a file, restarting a service, or running a command.

### Roles
Roles are a way to organize and package related tasks, variables, and files into reusable units. They provide a structured approach to breaking down complex Ansible playbooks into smaller, more manageable components. They allow to achieve modularization and code reusability and can be used by one or more playbooks.
