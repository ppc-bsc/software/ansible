# Kubernetes project
Project that installs the necessary software and deploys a Kubernetes cluster with containerd as CRI in the machines specified in the inventories.

## Inventories
Each partner should create its own inventories depending on their infrastructure. Nevertheless, there is a template file in the `inventories` folder. 
> More info: https://docs.ansible.com/ansible/latest/getting_started/get_started_inventory.html

## Roles
The roles defined are:
- clean-network: Deletes CNI network interface
- configure-containerd: Configures containerd with systemd enabled
- configure-node: Configures network for Kubernetes
- delete-cni: Deletes CNI binaries
- delete-kubernetes: Deletes kubeadm, kubelet and kubectl
- fetch-kubernetes-config: Copies kubectl config file 
- install-cni: Installs CNI binaries
- install-containerd: Installs containerd
- install-flannel: Installs the Flannel CNI plugin in a cluster
- install-kubernetes: Installs kubeadm, kubelet and kubectl
- kubeadm-init: Bootstraps a Kubernetes cluster
- kubeadm-join: Adds a worker to a cluster
- kubeadm-reset: Deletes a cluster

## Playbooks
Playbooks are composed by one or more roles, and may take configuration parameters. There are two types of playbooks defined:
- Installation
    - delete-software: Deletes all software installed for Kubernetes
    - install-software: Installs dependencies and Kubernetes on a machine. 
        > Check the parameters specified (sw version) before executing the playbook.
        - **Two roles can be chosen for containerd: installation or configuration** depending whether you already have containerd/Docker previously installed (then choose configure-containerd) or not (install-containerd)
- Deployment
    - create-cluster: Creates a Kubernetes cluster. When executed, it needs two parameters
        - *pod_subnet*. **IT HAS TO BE SPECIFIED IN THE COMMAND.**
        - *k8s_version*. By default, v1.29.5. **It has to match the installation in the machines**.
    - delete-cluster: Deletes a Kubernetes cluster
    - join-cluster: Adds Kubernetes worker(s) to a cluster

## Execution

### Install software
Install the necessary software to create a Kubernetes cluster in the machines set in the inventory. Set `$USER` to your machine's user.
> Before executing this playbook, check that the CNI and Kubernetes versions that are going to be installed are correct for you.

```
ansible-playbook \
        -i inventories/edge1.yaml \
        playbooks/install-software.yaml \
        -u $USER \
        --ask-become-pass
```

### Create cluster
Create a Kubernetes cluster set in the inventory. **It is important to set the pod_subnet variable!** In this case, it corresponds to the Flannel subnet. 
Set `$USER` to your machine's user.
```
ansible-playbook \
        -i inventories/edge1.yaml \
        playbooks/create-cluster.yaml \
        --extra-vars "pod_subnet=10.244.0.0/16" \
        -u $USER \
        --ask-become-pass
```

### Join cluster
Second, add the worker nodes specified in the same inventory. Set `$USER` to your machine's user. 
```
ansible-playbook \
        -i inventories/edge1.yaml \
        playbooks/join-cluster.yaml \
        -u $USER \
        --ask-become-pass
```

### Delete cluster
Run the following command to delete the cluster set in the inventory. Set `$USER` to your machine's user. 
```
ansible-playbook \
        -i inventories/edge1.yaml \
        playbooks/delete-cluster.yaml \
        -u $USER \
        --ask-become-pass
```

### Uninstall software
Uninstall the software installed to create a Kubernetes cluster in the machines set in the inventory. Set `$USER` to your machine's user. 
```
ansible-playbook \
        -i inventories/edge1.yaml \
        playbooks/delete-software.yaml \
        -u $USER \
        --ask-become-pass
```